# Customer technical test

## End points

### Add a new account with a customer 

Request URL: http://localhost:8080/account/

Request Method: POST

#### Example

##### Request:
```
POST http://localhost:8080/account/
Accept: */*
Cache-Control: no-cache
Content-Type: application/json
{"accountNumber":"123456","forename":"fred","surname":"smith","dateOfBirth":"1970-03-18"}
```

##### Response:
Headers:
```
HTTP/1.1 201 
Location: http://localhost:8080/account/customer/1
Content-Length: 0
```

### Lookup a customer/s using an accountId

Request URL: http://localhost:8080/customer/account/{accountId}

Request Method: GET

#### Example

##### Request:
```
GET http://localhost:8080/customer/account/1
Accept: */*
Cache-Control: no-cache
Content-Type: application/json
```

##### Response:
Headers:
```
HTTP/1.1 200 
Content-Type: application/json
Transfer-Encoding: chunked
Date: Tue, 28 Jan 2020 10:15:14 GMT
Keep-Alive: timeout=60
Connection: keep-alive
```
Body:
```
[
  {
    "customerId": 1,
    "accountIds": [
      "1"
    ],
    "forename": "fred",
    "surname": "smith",
    "dateOfBirth": "1970-03-18"
  }
]
```

### Lookup an account/s using a customerId

Request URL: http://localhost:8080/account/customer/{customerId}

Request Method: GET

### Example

#### Request:
```
GET http://localhost:8080/account/customer/1
```

#### Response:
Headers:
```
HTTP/1.1 200
Content-Type: application/json
Transfer-Encoding: chunked
Date: Tue, 28 Jan 2020 11:02:06 GMT
Keep-Alive: timeout=60
Connection: keep-alive
```
Body:
```
[
  {
    "accountId": 1,
    "customerIds": [
      "1"
    ],
    "accountNumber": 123456
  }
]
```