package uk.co.andyfennell.customerrestapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uk.co.andyfennell.customerrestapi.model.AccountEntity;

import java.util.List;
import java.util.Optional;

public interface AccountRepository extends JpaRepository<AccountEntity, Long> {

    Optional<AccountEntity> findAccountByAccountNumber(Integer accountNumber);

    @Query(value = "select a from AccountEntity a join a.customers c where c.customerId = :customerId")
    List<AccountEntity> findAccountByCustomerId(Long customerId);
}
