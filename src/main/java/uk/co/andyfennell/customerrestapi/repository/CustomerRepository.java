package uk.co.andyfennell.customerrestapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uk.co.andyfennell.customerrestapi.model.CustomerEntity;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface CustomerRepository extends JpaRepository<CustomerEntity, Long> {

    Optional<CustomerEntity> findCustomerByForenameAndSurnameAndDateOfBirth(String forename, String surname, Date dateOfBirth);

    @Query(value = "select c from CustomerEntity c join c.accounts a where a.accountId = :accountId")
    List<CustomerEntity> findCustomerByAccountId(Long accountId);
}
