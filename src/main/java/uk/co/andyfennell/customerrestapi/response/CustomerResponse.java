package uk.co.andyfennell.customerrestapi.response;

import java.util.Date;
import java.util.List;

public class CustomerResponse {

    private Long customerId;
    private List<String> accountIds;
    private String forename;
    private String surname;
    private Date dateOfBirth;

    public CustomerResponse(Long customerId, List<String> accountIds, String forename, String surname, Date dateOfBirth) {
        this.customerId = customerId;
        this.accountIds = accountIds;
        this.forename = forename;
        this.surname = surname;
        this.dateOfBirth = dateOfBirth;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public List<String> getAccountIds() {
        return accountIds;
    }

    public String getForename() {
        return forename;
    }

    public String getSurname() {
        return surname;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }
}
