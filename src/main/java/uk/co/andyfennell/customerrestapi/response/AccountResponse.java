package uk.co.andyfennell.customerrestapi.response;

import java.util.List;

public class AccountResponse {

    private Long accountId;
    private List<String> customerIds;
    private Integer accountNumber;

    public AccountResponse(Long accountId, List<String> customerIds, Integer accountNumber) {
        this.accountId = accountId;
        this.customerIds = customerIds;
        this.accountNumber = accountNumber;
    }

    public Long getAccountId() {
        return accountId;
    }

    public List<String> getCustomerIds() {
        return customerIds;
    }

    public Integer getAccountNumber() {
        return accountNumber;
    }
}
