package uk.co.andyfennell.customerrestapi.service;

import org.springframework.stereotype.Service;
import uk.co.andyfennell.customerrestapi.model.AccountEntity;
import uk.co.andyfennell.customerrestapi.model.CustomerEntity;
import uk.co.andyfennell.customerrestapi.exception.AccountAlreadyExistsException;
import uk.co.andyfennell.customerrestapi.repository.AccountRepository;
import uk.co.andyfennell.customerrestapi.repository.CustomerRepository;
import uk.co.andyfennell.customerrestapi.request.CustomerAccountRequest;

import java.util.List;

@Service
public class CustomerAccountService {

    private final AccountRepository accountRepository;
    private final CustomerRepository customerRepository;

    public CustomerAccountService(AccountRepository accountRepository, CustomerRepository customerRepository) {
        this.accountRepository = accountRepository;
        this.customerRepository = customerRepository;
    }

    public long save(final CustomerAccountRequest customerAccountRequest) {
        accountRepository.findAccountByAccountNumber(customerAccountRequest.getAccountNumber())
                .ifPresent(account -> {throw new AccountAlreadyExistsException(
                        String.format("Account number already exists:%s", customerAccountRequest.getAccountNumber()));});

        AccountEntity account = new AccountEntity(customerAccountRequest.getAccountNumber());
        account = accountRepository.save(account);

        CustomerEntity customer = customerRepository
                .findCustomerByForenameAndSurnameAndDateOfBirth(
                        customerAccountRequest.getForename(), customerAccountRequest.getSurname(), customerAccountRequest.getDateOfBirth())
                .orElseGet(
                        () -> new CustomerEntity(customerAccountRequest.getForename(), customerAccountRequest.getSurname(), customerAccountRequest.getDateOfBirth()));

        customer.getAccounts().add(account);
        customer = customerRepository.save(customer);

        return customer.getCustomerId();
    }

    public List<AccountEntity> findByCustomerId(final Long customerId) {
        return accountRepository.findAccountByCustomerId(customerId);
    }

    public List<CustomerEntity> findByAccountId(final Long accountId) {
        return customerRepository.findCustomerByAccountId(accountId);
    }
}
