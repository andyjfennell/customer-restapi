package uk.co.andyfennell.customerrestapi.controller;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;
import uk.co.andyfennell.customerrestapi.exception.AccountAlreadyExistsException;
import uk.co.andyfennell.customerrestapi.model.AccountEntity;
import uk.co.andyfennell.customerrestapi.model.CustomerEntity;
import uk.co.andyfennell.customerrestapi.request.CustomerAccountRequest;
import uk.co.andyfennell.customerrestapi.response.AccountResponse;
import uk.co.andyfennell.customerrestapi.response.CustomerResponse;
import uk.co.andyfennell.customerrestapi.response.ErrorResponse;
import uk.co.andyfennell.customerrestapi.service.CustomerAccountService;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class CustomerAccountController {

    private final CustomerAccountService customerAccountService;

    public CustomerAccountController(CustomerAccountService customerAccountService) {
        this.customerAccountService = customerAccountService;
    }

    @PostMapping("/account")
    public ResponseEntity<Void> addCustomer(@RequestBody CustomerAccountRequest customerAccount, UriComponentsBuilder builder) {
        long customerId = customerAccountService.save(customerAccount);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(builder.path("/account/customer/{customerId}").buildAndExpand(customerId).toUri());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    @GetMapping("/account/customer/{customerId}")
    public List<AccountResponse> lookupAccountByCustomerId(@PathVariable Long customerId) {
        List<AccountEntity> accounts = customerAccountService.findByCustomerId(customerId);
        return buildLookupAccountResponse(accounts);
    }

    @GetMapping("/customer/account/{accountId}")
    public List<CustomerResponse> lookupCustomerByAccountId(@PathVariable Long accountId) {
        List<CustomerEntity> customers = customerAccountService.findByAccountId(accountId);
        return buildLookupCustomerResponse(customers);
    }

    @ExceptionHandler({AccountAlreadyExistsException.class})
    public ResponseEntity<ErrorResponse> handleException(Exception ex) {
        return new ResponseEntity<>(new ErrorResponse(HttpStatus.CONFLICT.value(), ex.getMessage()), HttpStatus.CONFLICT);
    }

    private List<AccountResponse> buildLookupAccountResponse(List<AccountEntity> accounts) {
        List<AccountResponse> response = new ArrayList<>();
        accounts.forEach(account -> {
            List<String> customerIds = account.getCustomers().stream().map(customer -> String.valueOf(customer.getCustomerId())).collect(Collectors.toList());
            response.add(new AccountResponse(account.getAccountId(), customerIds, account.getAccountNumber()));
        });
        return response;
    }

    private List<CustomerResponse> buildLookupCustomerResponse(List<CustomerEntity> customers) {
        List<CustomerResponse> response = new ArrayList<>();
        customers.forEach(customer -> {
            List<String> accountIds = customer.getAccounts().stream().map(account -> String.valueOf(account.getAccountId())).collect(Collectors.toList());
            response.add(new CustomerResponse(customer.getCustomerId(), accountIds, customer.getForename(), customer.getSurname(), customer.getDateOfBirth()));
        });
        return response;
    }
}
